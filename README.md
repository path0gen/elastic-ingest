# Elastic-Ingest

## Description
Elasticsearch ingest pipelines for converting various data sources to Elastic Common Schema (ECS) format.

## Data Source Roadmap
- [ ] Bricata **(in progress)**
    - [x] conn
    - [x] dns
    - [x] http
    - [x] files
    - [x] x509
    - [x] ssl
    - [ ] ntp
    - [ ] smtp
    - [ ] weird
    - [ ] intel
    - [ ] mwd
    - [ ] dhcp
    - [ ] software
    - [ ] observed_users
    - [ ] ftp
    - [ ] ssh
    - [ ] pe
    - [ ] smb_mapping
    - [ ] kerberos
    - [ ] dce_rpc
    - [ ] ntlm
    - [ ] smb_files
    - [ ] irc
    - [ ] notice
    - [ ] snmp
    - [ ] traceroute
    - [ ] tunnel
    - [ ] rdp
    - [ ] dpd
    - [ ] others?
- [ ] Bluecoat
- [ ] McAfee ePO

## Authors and acknowledgment
Inspired by https://github.com/corelight/ecs-mapping.
