{
    "processors": [
        {
            "append": {
                "field": "_ingest.pipelines",
                "value": [
                    "bricata-{{event.dataset}}"
                ]
            }
        },
        {
            "set": {
                "field": "event.category",
                "value": "network",
                "ignore_failure": true
            }
        },
        {
            "dot_expander": {
                "field": "id.orig_p",
                "path": "bro_log",
                "ignore_failure": true
            }
        },
        {
            "dot_expander": {
                "field": "id.orig_h",
                "path": "bro_log",
                "ignore_failure": true
            }
        },
        {
            "dot_expander": {
                "field": "id.resp_p",
                "path": "bro_log",
                "ignore_failure": true
            }
        },
        {
            "dot_expander": {
                "field": "id.resp_h",
                "path": "bro_log",
                "ignore_failure": true
            }
        },
        {
            "rename": {
                "field": "bro_log.proto",
                "target_field": "network.transport",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.id.orig_h",
                "target_field": "source.ip",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.id.orig_p",
                "target_field": "source.port",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.id.resp_h",
                "target_field": "destination.ip",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.id.resp_p",
                "target_field": "destination.port",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.AA",
                "target_field": "dns.flags.authoritative",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.RA",
                "target_field": "dns.flags.recursion.available",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.RD",
                "target_field": "dns.flags.recursion.desired",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "set": {
                "field": "event.outcome",
                "value": "failure",
                "ignore_failure": true,
                "if": "(ctx.bro_log?.rejected == true)"
            }
        },
        {
            "set": {
                "field": "event.outcome",
                "value": "success",
                "ignore_failure": true,
                "if": "(ctx.bro_log?.rejected == false)"
            }
        },
        {
            "rename": {
                "field": "bro_log.rejected",
                "target_field": "dns.flags.rejected",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.TC",
                "target_field": "dns.flags.truncated.response",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.Z",
                "target_field": "dns.flags.z_bit",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.qclass",
                "target_field": "dns.question.class_code",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.qclass_name",
                "target_field": "dns.question.class",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.qtype",
                "target_field": "dns.question.type_code",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.qtype_name",
                "target_field": "dns.question.type",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "set": {
                "field": "dns.question.name",
                "value": "{{bro_log.query}}",
                "if": "ctx.bro_log?.query != null || ctx.bro_log?.query != ''",
                "ignore_failure": true
            }
        },
        {
            "script": {
                "lang": "painless",
                "source": "ctx.dns.question.name_length = ctx.bro_log?.query.length()",
                "if": "ctx.bro_log?.query != null || ctx.bro_log?.query != ''",
                "ignore_failure": true
            }
        },
        {
            "rename": {
                "field": "bro_log.query",
                "target_field": "destination.domain",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "script": {
                "lang": "painless",
                "source": "ctx.dns.question.registered_domain = ctx.destination.domain.substring(ctx.destination.domain.indexOf('.')+1)",
                "ignore_failure": true
            }
        },
        {
            "rename": {
                "field": "bro_log.rcode_name",
                "target_field": "dns.response_code",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.rtt",
                "target_field": "dns.rtt",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "script": {
                "lang": "painless",
                "source": "ctx.event.duration = (long)(ctx.dns.rtt * params.param_c)",
                "params": {
                    "param_c": 1000000
                },
                "ignore_failure": true
            }
        },
        {
            "remove": {
                "field": "bro_log.rtt",
                "ignore_failure": true
            }
        },
        {
            "rename": {
                "field": "bro_log.rcode",
                "target_field": "dns.status_code",
                "ignore_failure": true
            }
        },
        {
            "rename": {
                "field": "bro_log.trans_id",
                "target_field": "dns.id",
                "ignore_failure": true
            }
        },
        {
            "append": {
                "field": "dns.header_flags",
                "value": "AA",
                "ignore_failure": true,
                "if": "(ctx.dns.AA == true)"
            }
        },
        {
            "append": {
                "field": "dns.header_flags",
                "value": "RA",
                "ignore_failure": true,
                "if": "(ctx.dns.RA == true)"
            }
        },
        {
            "append": {
                "field": "dns.header_flags",
                "value": "RD",
                "ignore_failure": true,
                "if": "(ctx.dns.RD == true)"
            }
        },
        {
            "append": {
                "field": "dns.header_flags",
                "value": "TC",
                "ignore_failure": true,
                "if": "(ctx.dns.TC == true)"
            }
        },
        {
            "rename": {
                "field": "bro_log.uid",
                "target_field": "event.uid",
                "ignore_failure": true,
                "ignore_missing": true
            }
        }
    ]
}