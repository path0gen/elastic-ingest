{
    "processors": [
        {
            "append": {
                "field": "_ingest.pipelines",
                "value": [
                    "bricata-{{event.dataset}}"
                ]
            }
        },
        {
            "set": {
                "field": "event.category",
                "value": "network",
                "ignore_failure": true
            }
        },
        {
            "dot_expander": {
                "field": "id.orig_p",
                "path": "bro_log",
                "ignore_failure": true
            }
        },
        {
            "dot_expander": {
                "field": "id.orig_h",
                "path": "bro_log",
                "ignore_failure": true
            }
        },
        {
            "dot_expander": {
                "field": "id.resp_p",
                "path": "bro_log",
                "ignore_failure": true
            }
        },
        {
            "dot_expander": {
                "field": "id.resp_h",
                "path": "bro_log",
                "ignore_failure": true
            }
        },
        {
            "rename": {
                "field": "bro_log.proto",
                "target_field": "network.transport",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.id.orig_h",
                "target_field": "source.ip",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.id.orig_p",
                "target_field": "source.port",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.id.resp_h",
                "target_field": "destination.ip",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.id.resp_p",
                "target_field": "destination.port",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.orig_bytes",
                "target_field": "source.bytes",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.orig_ip_bytes",
                "target_field": "source.ip_bytes",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.orig_pkts",
                "target_field": "source.packets",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.orig_l2_addr",
                "target_field": "source.mac",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.resp_pkts",
                "target_field": "destination.packets",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.resp_ip_pkts",
                "target_field": "destination.ip_packets",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.resp_bytes",
                "target_field": "destination.bytes",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.resp_ip_bytes",
                "target_field": "destination.ip_bytes",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.resp_l2_addr",
                "target_field": "destination.mac",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.direction",
                "target_field": "network.direction",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "convert": {
                "field": "duration",
                "type": "float",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "script": {
                "lang": "painless",
                "source": "ctx.event.duration = (long)(ctx.bro_log?.duration * params.param_c)",
                "params": {
                    "param_c": 1000000
                },
                "ignore_failure": true
            }
        },
        {
            "remove": {
                "field": "bro_log.duration",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.history",
                "target_field": "network.connection.history",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.community_id",
                "target_field": "network.community_id",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.service",
                "target_field": "network.protocol",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.missed_bytes",
                "target_field": "network.missed_bytes",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "network_direction",
                "target_field": "network.direction",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.local_orig",
                "target_field": "network.connection.local_orig",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.local_resp",
                "target_field": "network.connection.local_resp",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "enrich": {
                "policy_name": "bricata-conn-state-enrichment-policy",
                "field": "bro_log.conn_state",
                "target_field": "temporary_field_rename_conn_enrich",
                "max_matches": "1"
            }
        },
        {
            "rename": {
                "field": "temporary_field_rename_conn_enrich.state",
                "target_field": "network.connection.state",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "temporary_field_rename_conn_enrich.state_description",
                "target_field": "network.connection.state_description",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "remove": {
                "field": "temporary_field_rename_conn_enrich",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "remove": {
                "field": "bro_log.conn_state",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.in_iface",
                "target_field": "network.interface",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.uid",
                "target_field": "event.uid",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.irt",
                "target_field": "zeek.irt",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.clisrv_id",
                "target_field": "zeek.clisrv_id",
                "ignore_failure": true,
                "ignore_missing": true
            }
        },
        {
            "rename": {
                "field": "bro_log.pcr",
                "target_field": "zeek.pcr",
                "ignore_failure": true,
                "ignore_missing": true
            }
        }
    ]
}